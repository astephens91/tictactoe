//Global Variables

let currentPlayer = 'X';
let playerXSelections = [];
let playerOSelections = [];

const winningCombinations = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
    [1, 4, 7],
    [2, 5, 8],
    [3, 6, 9],
    [1, 5, 9],
    [3, 5, 7]
];

let gameOver = false


//Event Handler
// get all td elements from the DOM and store in cellElementArray
const cellElementArray = document.querySelectorAll('td');
// write for loop to iterate over cellElementArray
for (let i = 0; i < cellElementArray.length; i++) {
    // set cellElementArray[i] to currentCell variable
    let currentCell = cellElementArray[i]
    // add an event listener to the currentCell
    currentCell.addEventListener('click', function(event){
        if(gameOver === false){
        const clickedCellElement = event.target;
        // console log the id of the cell being clicked on
        if(currentPlayer === 'X'){
            document.getElementById(clickedCellElement.id).innerHTML = 'X'
            playerXSelections.push(clickedCellElement.id)
            currentPlayer = 'O'
            document.getElementById("turn").innerHTML = currentPlayer

            if(checkForWin(winningCombinations, playerXSelections) == true){
                window.alert("X Wins!")
                gameOver = true
                document.getElementById("message").innerHTML = "THE GAME IS OVER BROTHER"
            }
        } else {
            document.getElementById(clickedCellElement.id).innerHTML = 'O'
            playerOSelections.push(clickedCellElement.id)
            currentPlayer = 'X'
            document.getElementById("turn").innerHTML = currentPlayer

            if(checkForWin(winningCombinations, playerOSelections) == true){
                window.alert("O Wins!")
                gameOver = true
                document.getElementById("message").innerHTML = "THE GAME IS OVER BROTHER"
            }
        }
    }

    });
}

function checkForWin(winningCombination, playerSelections){
    for(i=0; i<winningCombination.length; i++){
        let matches = 0
        for(x=0; x<winningCombination[i].length; x++){
            if(playerSelections.includes(winningCombination[i][x].toString())){
                matches += 1
            }

        }
        if(matches === 3){
            return true
        }
    }
    return false
}

function reset(){
    gameOver = false
    playerOSelections = []
    playerXSelections = []
    currentPlayer = 'X'
    document.getElementById("turn").innerHTML = currentPlayer
    
    for (let i = 0; i < cellElementArray.length; i++) {
        // set cellElementArray[i] to currentCell variable
        let currentCell = cellElementArray[i]
        currentCell.innerHTML = ""
    }

}